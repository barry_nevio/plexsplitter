package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"./lib/Error"
	"./models/MediaTimeStamp"
	"./models/Show"
)

func main() {

	// Get config
	ffmpegBin, err := getFfmpegBin()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// Open the JSON file
	jsonFile, err := os.Open("./shows/LittleBear.json")
	if err != nil {
		fmt.Println("CAN NOT OPEN FILE: " + err.Error())
		return
	}
	defer jsonFile.Close()

	// Read the JSON file
	jsonData, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println("CAN NOT READ FILE: " + err.Error())
		return
	}

	// Create a new show
	show := Show.New(jsonData)

	// Loop through the files
	for _, file := range show.Files {

		fmt.Println("CURRENTLY SPLITTING: " + file.File)

		for _, episode := range file.Episodes {

			// Calculate duration
			startMTS := MediaTimeStamp.NewFromStringTS(episode.Start)
			endMTS := MediaTimeStamp.NewFromStringTS(episode.End)
			if endMTS.SecondsTS <= startMTS.SecondsTS {
				fmt.Println("The end timestamp is less than or equal to the start timestamp! Can not continue")
				return
			}
			durationInSeconds := endMTS.SecondsTS - startMTS.SecondsTS

			durationMTS := MediaTimeStamp.NewFromSecondsTS(durationInSeconds)

			fmt.Println("CREATING EPISODE: " + episode.OutFileName)
			fmt.Println("TOTAL DURATION OF EPISODE: " + durationMTS.StringTS)
			cmd := exec.Command(ffmpegBin,
				"-ss",
				episode.Start,
				"-i",
				show.RootFolder+file.File,
				"-to",
				durationMTS.StringTS,
				"-c",
				"copy",
				"-y",
				show.RootFolder+episode.OutFileName,
			)
			//cmd.Stdout = os.Stdout
			//cmd.Stderr = os.Stderr
			err := cmd.Run()
			if err != nil {
				log.Fatalf("cmd.Run() failed with %s\n", err)
				return
			}

		}

	}

}

func getFfmpegBin() (string, error) {

	// Current dir
	dir, _ := os.Getwd()

	// Set the ffmpeg bins according to the OS
	switch strings.ToUpper(runtime.GOOS) {
	case "WINDOWS":
		return dir + "/ffmpeg/4.2.1/windows/bin/ffmpeg.exe", nil
		//ffmpegConfig.FfprobeBin = dir + "/ffmpeg/4.1.4/windows/bin/ffprobe.exe"
	case "LINUX":
		return dir + "/ffmpeg/4.1.4/linux/ffmpeg", nil
		//ffmpegConfig.FfprobeBin = dir + "/ffmpeg/4.1.4/linux/ffprobe"
	case "DARWIN":
		return dir + "/ffmpeg/4.2.1/macos/bin/ffmpeg", nil
		//ffmpegConfig.FfprobeBin = dir + "/ffmpeg/4.1.4/macos/bin/ffprobe"
	default:
		return "", Error.New("CAN NOT DETECT YOUR OS")
	}

}
