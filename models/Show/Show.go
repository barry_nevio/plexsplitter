package Show

import "encoding/json"

type Context struct {
	RootFolder string `json:"root_folder"`
	Files      []struct {
		File     string `json:"file"`
		Episodes []struct {
			OutFileName string `json:"out_file_name"`
			Start       string `json:"start"`
			End         string `json:"end"`
		} `json:"episodes"`
	} `json:"files"`
}

func New(jsonData []byte) Context {

	c := Context{}

	_ = json.Unmarshal(jsonData, &c)

	return c
}
