package MediaTimeStamp

import (
	"strings"

	"github.com/spf13/cast"
)

type Context struct {
	Hour      int
	Minute    int
	Second    int
	Frame     int
	SecondsTS int
	StringTS  string
}

func NewFromStringTS(stringTS string) Context {

	c := Context{}
	c.StringTS = stringTS

	// Remove nano seconds and get the time segments
	frameSplit := strings.Split(c.StringTS, ".")
	timeSplit := strings.Split(frameSplit[0], ":")

	// Frame
	if len(frameSplit) > 1 {
		c.Frame = cast.ToInt(removePrependedZeros(frameSplit[1]))
	}

	// Hours
	if len(timeSplit) > 0 {
		c.Hour = cast.ToInt(removePrependedZeros(timeSplit[0]))
	}

	// Minutes
	if len(timeSplit) > 1 {
		c.Minute = cast.ToInt(removePrependedZeros(timeSplit[1]))
	}

	// Second
	if len(timeSplit) > 2 {
		c.Second = cast.ToInt(removePrependedZeros(timeSplit[2]))
	}

	hoursInSeconds := c.Hour * 3600
	minutesInSeconds := c.Minute * 60
	c.SecondsTS = hoursInSeconds + minutesInSeconds + c.Second

	return c

}

func NewFromSecondsTS(secondsTS int) Context {

	c := Context{}
	c.SecondsTS = secondsTS

	if c.SecondsTS > 0 && c.SecondsTS >= 60 {

		c.Second = c.SecondsTS % 60
		c.Minute = c.SecondsTS / 60
		if c.Minute >= 60 {
			c.Hour = c.Minute / 60
			c.Minute = c.Minute % 60
		}

	}

	c.StringTS = intToDoubleDigitString(c.Hour) + ":" + intToDoubleDigitString(c.Minute) + ":" + intToDoubleDigitString(c.Second) + "." + intToTripleDigitString(c.Frame)

	return c
}

func intToDoubleDigitString(i int) string {

	asString := cast.ToString(i)

	if len(asString) == 0 {
		return "00"
	}

	if len(asString) == 1 {
		return "0" + asString
	}

	return asString
}

func intToTripleDigitString(i int) string {

	asString := cast.ToString(i)

	if len(asString) == 0 {
		return "000"
	}

	if len(asString) == 1 {
		return "00" + asString
	}

	if len(asString) == 2 {
		return "0" + asString
	}

	return asString
}

func removePrependedZeros(ds string) string {

	rs := []rune(ds)
	var fs string
	var noMoreLeadingZeros bool
	for _, char := range rs {
		if string(char) != "0" && !noMoreLeadingZeros {
			noMoreLeadingZeros = true
		}
		if noMoreLeadingZeros {
			fs = fs + string(char)
		}
	}

	return fs

}
